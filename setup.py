"""
CREATED AT: 2019/07

Configuration for python packaging,
Defines a simple package for the application. 
"""
from setuptools import setup

with open("requirements.txt") as f:
    requirements = f.read().splitlines()

setup(
    name="monkey-scrapper",
    version="0.0.1",
    install_requires=requirements,
    description="Web Scraper using scrapy",
    license="MIT",
    packages=["scraper"],
    author="Pedro Frattezi",
    author_email="pedro.frattezi@gmail.com",
    keywords=["scraper"],
    url="https://gitlab.com/frattezi/monkey-scraper",
)
